import React, { Component } from 'react';
import {
    View,
    FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import {
    ModalWrapper
} from '../../components/commons';
import {
    StyledButton
} from '../../components/elements';

class CheckModal extends Component{
    render(){
        const { 
            title,
            content,
            type = "column",
            data,
            onSelect
        } = this.props;

        return(
            <ModalWrapper 
                title={title} 
                content={content}
                style={[styles.container,{flexDirection: type == "column" ? "column" : 'row-reverse'}]} 
            >
            <FlatList 
                data={data}
                renderItem={({item}) => (
                    <StyledButton 
                        type="select"
                        backgroundColor={EStyleSheet.value("$second_color")}
                        title={item.name}
                        style={styles.buttons}
                        onPress={() => onSelect(item)}
                    />
                )}
                keyExtractor={(item) => String(item.id)}
                style={{
                    width: '100%',
                    maxHeight: 300
                }}
            />
            </ModalWrapper>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        alignItems: 'center',
        paddingBottom: 10,
        justifyContent: 'center'
    },
    buttons: {
        width: '100%',
        marginVertical: 5
    },
    rowButton: {
        margin: 10,
        width: '40%'
    },
    columnButton: {
        margin: 5,
        width: '90%'
    }
})

export { CheckModal }
