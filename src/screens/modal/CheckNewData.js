import React, { Component } from 'react';
import {
    View,
    Image,
    Dimensions,
    Linking
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import {
    ModalWrapper, MText
} from '../../components/commons';
import {
    StyledButton
} from '../../components/elements';
import { navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");

class CheckNewData extends Component{
    handleLink = (link) => {
        if(!link) return false

        if(link.slice(0, 4) === "http"){
            Linking.openURL(link);
        } else {
            const slice = link.split("&");
            let props = {};
            slice.map((item,index) => {
                if(index != 0){
                    const itemsplit = item.split("=");
                    props[ itemsplit[0] ] = itemsplit[1];
                }
            })
            navHelper.push({
                screen: slice[0],
                passProps: {...props}
            });
        }
    }
    

    render(){
        const { 
            title,
            content,
            data
        } = this.props;

        return(
            <ModalWrapper 
                title={title} 
                content={data.content}
                style={styles.container} 
            >
            {data.pic ? (
                <Image 
                    source={{uri: `http://kalantari.almaseshomal.com/Mehr/${data.pic}`}}
                    style={{
                        width: width*8/10,
                        height: width*4/10,
                        borderRadius: 10,
                        marginBottom: 10
                    }}
                />
            ) : null}
            
            {data.linkText ? (
                <StyledButton 
                    type="select"
                    backgroundColor={EStyleSheet.value("$second_color")}
                    title={item.linkText}
                    style={styles.buttons}
                    onPress={() => this.handleLink(data.link)}
                />
            ) : null}
            </ModalWrapper>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        alignItems: 'center',
        paddingBottom: 10,
        justifyContent: 'center',
        flexDirection : 'column'
    },
    buttons: {
        width: '100%',
        marginVertical: 5
    },
    rowButton: {
        margin: 10,
        width: '40%'
    },
    columnButton: {
        margin: 5,
        width: '90%'
    }
})

export { CheckNewData }
