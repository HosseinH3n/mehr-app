import React, { PureComponent } from "react";
import {
    View,
    Dimensions,
    Platform
} from 'react-native';
//import my components
import { MText } from '../components/commons';
//variables
const { width } = Dimensions.get("screen")

class Notification extends PureComponent{
    render(){
        const { text, type = "error" } = this.props;
        return(
            <View style={{
                width,
                padding: 15,
                backgroundColor: type === "success" ? "#85D235" : "#FF7676",
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: Platform.select({
                    ios: 40,
                    android: 10
                })

            }}>
                <MText style={{
                    color: '#fff'
                }} >{text}</MText>
            </View>
        )
    }
}

export {Notification};