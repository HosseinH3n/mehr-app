import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Image, 
	Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//compoenents
import { PageWrapper, Input, SelectBox } from '../../components/commons';
import { StyledButton } from '../../components/elements';
import { numbers, navHelper } from '../../utils';
import { observer } from 'mobx-react';
import { registerStore } from '../../stores';
//variables
const { width } = Dimensions.get("screen");

@observer
class ClientPresent extends Component{    
	state = {
		loading: true
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				this.setState({
					loading: false
				})
                break;
            case 'willDisappear':
                break;
        }
	}

	register = () => {
		registerStore.handleClient().then(() => {
			navHelper.setNotif({
				text: "ممنون از معرفی شما ",
				type: 'success',
				time: 1
			});
			navHelper.pop();
		}).catch(err => {
			navHelper.setNotif({
				text: err
			})
		})
	}

    render(){
        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				loading={this.state.loading}
				title="معرفی مددجو"
				style={{flex: 1}}
            >
				<KeyboardAwareScrollView 
					showsVerticalScrollIndicator={false}
					keyboardShouldPersistTaps={"always"} 
					contentContainerStyle={styles.container} >
					<Input 
						placeholder="نام معرفی کننده"
						handleChange={(text) => registerStore.fillClient("presenter_name",text)}
					/>
					<Input 
						placeholder="شماره همراه"
						keyboardType="numeric"
						handleChange={(text) => registerStore.fillClient("presenter_tell",text)}
					/>
					<Input 
						placeholder="نام و نام خانوادگی مددجو"
						handleChange={(text) => registerStore.fillClient("fullname",text)}
					/>
					<Input 
						placeholder="شماره تماس مددجو (اختیاری)"
						keyboardType="numeric"
						handleChange={(text) => registerStore.fillClient("tell",text)}
					/>
					<Input 
						placeholder="آدرس مددجو را بنویسید"
						style={{height: 80}}
						multiline={true}
						handleChange={(text) => registerStore.fillClient("address",text)}
					/>
					<SelectBox 
						title="نیازمندی حمایتی"
						content="مددجو به چه موردی نیازمند است. اگر نیازمند چند مورد یا موارد دیگری هست در بخش بعدی بنویسید "
						data={[{
							id :1,
							name :'معیشتی'
						},{
							id :2,
							name :'درمان'
						},{
							id :3,
							name :'ازدواج'
						},{
							id :4,
							name :'جهیزیه'
						},{
							id :5,
							name :'مسکن'
						},{
							id :6,
							name :'اشتغال'
						}]}
						onSelect={(id) => registerStore.fillClient("needing",id)}
					/>
					<Input 
						placeholder="لطفا مددجو را در چند خط معرفی کنید (اختیاری)"
						style={{height: 120}}
						multiline={true}
						handleChange={(text) => registerStore.fillClient("description",text)}
					/>
					<View style={{
						width: '100%',
						justifyContent: 'center',
						alignItems:'center'
					}} >
						<StyledButton
							title="معرفی مددجو"
							icon="madadjo"
							backgroundColor={EStyleSheet.value("$first_colorful")}
							iconColor={EStyleSheet.value("$dark_color")}
							iconSize={24}
							width={width*8/10}
							onPress={this.register}
						/>
					</View>
				</KeyboardAwareScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
		marginHorizontal: '5%',
		paddingTop: 10
	}
})

export { ClientPresent }
