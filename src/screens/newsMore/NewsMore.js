import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Image, 
	Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import axios from 'axios';
import Svg,{
    LinearGradient,
    Rect,
    Defs,
    Stop
} from 'react-native-svg';
//compoenents
import { PageWrapper, MIcon, MText } from '../../components/commons'
import { numbers } from '../../utils';
import { CardSlider, ImageSlide } from '../../components/elements';
import { homeStore } from '../../stores';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width*9/10;
const ITEM_HEIGHT = ITEM_WIDTH/2;

class NewsMore extends Component{    
	state = {
		loading: true,
		id: null,
		"pic": "",
        "title": "",
        "content": "",
        "date": "",
        "address": "",
        "category_id": null,
        "gallery": []
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				const fd = new FormData();
				fd.append("id",this.props.id);
				axios.post("/events",fd).then(({data}) => {
					if(data.code === 200){
						this.setState({
							...data.events,
							loading: false
						});
					}
				}).catch(err => false)
                break;
            case 'willDisappear':
                break;
        }
	}

    render(){
		const { title, pic, category_id, content, date, address, gallery } = this.state; 
		const category = category_id ? homeStore.categories.filter(item => item.id == category_id)[0].title : null;
        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				loading={this.state.loading}
				title={this.props.title.substring(0,30)+"..."}
            >
				<ScrollView 
				showsVerticalScrollIndicator={false}
				contentContainerStyle={styles.container}
				style={{
					height: '100%'
				}} >
					<View style={styles.imageWrapper} >
						<Image 
							style={styles.image}
							source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
							resizeMode="cover"
						/>
						<View style={styles.absoluteWrapper} >
							<Svg
								height={ITEM_HEIGHT}
								width={ITEM_WIDTH}
							>
								<Defs>
									<LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
										<Stop offset="0%" stopColor="rgb(0,0,0)" stopOpacity="0" />
										<Stop offset="100%" stopColor="rgb(0,0,0)" stopOpacity="0.9" />
									</LinearGradient>
								</Defs>
								<Rect x="0" y="0" width={ITEM_WIDTH} height={ITEM_HEIGHT} fill="url(#grad)"  />
							</Svg>
						</View>
						<View style={[styles.absoluteWrapper,styles.textWrapper]} >
							<MText style={styles.titleText} type="reg_b" >{title}</MText>
						</View>
					</View>
					<View style={[styles.row,styles.lastRow]} >
						{category ? (
							<View style={styles.label} >
								<MIcon 
									name="layers"
									color={EStyleSheet.value("$second_color")}
									size={16}
								/>
								<MText style={styles.labelText} type="reg_m" >{category}</MText>
							</View>
						) : null}
						{date ? (
							<View style={styles.label} >
								<MIcon 
									name="calendar"
									color={EStyleSheet.value("$second_color")}
									size={16}
								/>
								<MText style={styles.labelText} type="reg_m" >{numbers.toPersianDigits(date)}</MText>
							</View>
						) : null}
						{address ? (
							<View style={styles.label} >
								<MIcon 
									name="placeholder"
									color={EStyleSheet.value("$second_color")}
									size={16}
								/>
								<MText style={styles.labelText} type="reg_m" >{address}</MText>
							</View>
						) : null}
					</View>
					<View style={styles.row} >
						<MText type="reg_b">{content}</MText>
					</View>
					<CardSlider 
						data={gallery}
						horizontal={false}
						renderItem={(item) => <ImageSlide key={item.id} {...item} />}
						title="گالری تصاویر"
						showMore={false}
					/>
				</ScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
		marginHorizontal: '5%',
		paddingTop: 10,
		paddingBottom: 30
	},
	imageWrapper: {
        width: ITEM_WIDTH,
        height:  ITEM_HEIGHT,
		position: 'relative',
		borderRadius: 10,
		overflow: 'hidden',
		marginBottom: 20
    },
    image: {
        width: '100%',
        height: '100%'
    },
    absoluteWrapper: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: 10,
        top: 0,
        right: 0
    },
    textWrapper: {
        zIndex: 20,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    titleText: {
        width: '100%',
        color: '$ligh_color'
    },
	row: {
		paddingHorizontal : 7,
		paddingVertical: 15
	},
	lastRow: {
		backgroundColor: '#f7f7f7',
		flexDirection: 'column',
		borderRadius: 10
	},
	label: {
		flexDirection: 'row-reverse',
		alignItems: 'center',
		marginVertical: 5,
		opacity: 0.7
	},
	labelText: {
		marginRight: 6,
		fontSize: 14
	}
})

export { NewsMore }
