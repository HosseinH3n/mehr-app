import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Dimensions,
	AsyncStorage
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { observer } from 'mobx-react';
//compoenents
import { PageWrapper } from '../../components/commons'
import { 
    Swiper, 
    CardSlider, 
	StyledButton,
	NewsCard,
	CampiganCard,
	CategoryCard,
	Menu
} from '../../components/elements';
import { homeStore } from '../../stores/homeStore';
import { navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");

@observer
class Home extends Component{
    componentDidMount = () => {
		homeStore.fetchData();
		homeStore.checkNewData().then(res => {
			if(res.state){
				AsyncStorage.getItem("checkNewData").then(val => {
					if(val){
						if(val*1 == res.id){
							return false
						} else {
							this.showModal(res)
						}
					} else {
						this.showModal(res)
					}
				}).catch(err => this.showModal(res))
			}
		});
	}

	showModal = (res) => {
		AsyncStorage.setItem("checkNewData",String(res.id));
		navHelper.openModal({
			screen: 'Mehr.Modal.CheckNewData',
			title: res.title,
			props: {
				data: {...res}
			}
		})
	}
	
	openRegister(){
		navHelper.push({
			screen: "Mehr.Register"
		})
	}
    
    render(){
        return(
			<PageWrapper 
				loading={homeStore.loading}
                navigator={this.props.navigator}
				type="home"
				menuComponent={(toggleAnimation) => <Menu toggleAnimation={toggleAnimation} />}
            >
				<ScrollView style={{flex: 1}} >
					<Swiper 
						data={homeStore.getSlides} 
					/>
					<CardSlider 
						data={homeStore.categories}
						renderItem={(item) => <CategoryCard key={item.id} {...item} />}
						title="کارگروه ها" 
						onPress = {() => false}
						showMore={false}
					/>
					<StyledButton 
						title="اگر نیازمندی میشناسید به ما معرفی کنید"
						icon="madadjo"
						iconSize={23}
						color={EStyleSheet.value("$second_color")}
						backgroundColor={EStyleSheet.value("$ligh_color")}
						type="select"
						style={{
							borderWidth: 1,
							borderColor: EStyleSheet.value("$thir_color"),
							marginHorizontal: 15,
							paddingVertical: 20
						}}
						onPress={() => {
							navHelper.push({
								screen: "Mehr.ClientPresent"
							});
						}}
					/>
					<CardSlider 
						data={homeStore.plans}
						renderItem={(item) => <CampiganCard key={item.id} {...item} />}
						title="طرح ها و پویش ها" 
						onPress = {() => false}
						showMore={false}
					/>
					<CardSlider 
						data={homeStore.events}
						renderItem={(item) => <NewsCard key={item.id} {...item} />}
						title="رویداد های اخیر" 
						onPress = {() => false}
						showMore={false}
					/>
					<View style={{
						width: '100%',
						justifyContent: 'center',
						alignItems:'center'
					}} >
						<StyledButton
							title="همین حالا مهربانی را هدیه کنید"
							icon="heemayat"
							iconSize={25}
							width={width*9/10}
							onPress={this.openRegister}
						/>
					</View>
				</ScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export { Home }
