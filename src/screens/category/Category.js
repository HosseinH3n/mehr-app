import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Dimensions,
	Image
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { observer } from 'mobx-react';
import axios from 'axios';
//compoenents
import { PageWrapper, MText } from '../../components/commons'
import { 
    Swiper, 
    CardSlider, 
	StyledButton,
	NewsCard,
	CampiganCard,
	CategoryCard
} from '../../components/elements';
import { homeStore } from '../../stores/homeStore';
import { navHelper, numbers } from '../../utils';
//variables
const { width } = Dimensions.get("screen");

@observer
class Category extends Component{
	state = {
		loading: true,
		 "id": null,
        "title": "",
        "pic": "",
        "manager_name": "",
        "manager_pic": "",
        "description": ""
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				const fd = new FormData();
				fd.append("id",this.props.id);
				axios.post("/categories",fd).then(({data}) => {
					if(data.code === 200){
						this.setState({
							...data.categories,
							loading: false
						});
					}
				}).catch(err => false)
                break;
            case 'willDisappear':
                break;
        }
	}

	openRegister = () => {
		navHelper.push({
			screen: "Mehr.Register"
		})
	}
	
    render(){
		const { title, pic, manager_name, manager_pic, description, members } = this.state; 
        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				title={`کارگروه ${this.props.title}`}
				loading={this.state.loading}
            >
					<View style={styles.header} >
						<Image 
							style={styles.image}
							source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
							resizeMode="cover"
						/>
						<MText type="bold_m" >{`کارگروه ${title}`}</MText>
						<MText style={styles.explainText} type="reg_m" >{description}</MText>
						<View style={{
							width: '100%',
							justifyContent: 'center',
							alignItems:'center'
						}} >
							<StyledButton
								title={`ثبت نام در کارگروه ${title}`}
								icon="heemayat"
								iconSize={25}
								width={width*9/10}
								onPress={this.openRegister}
							/>
						</View>
						<View style={styles.lastSection} >
							{members < 10 ? null : (
								<View style={styles.lastSectionColumn}>
									<MText type="reg_b" >تعداد اعضا :</MText>
									<MText type="bold_s">{numbers.toPersianDigits(members)} نفر</MText>
								</View>
							)}
							
							{members < 10 ? null : <View style={styles.line} />}
							<View style={styles.lastSectionColumn}>
								<MText type="reg_b" >مسئول بخش :</MText>
								<MText type="bold_s">{manager_name}</MText>
								<Image 
									resizeMode="cover"
									style={{width: 40,height: 40,borderRadius: 20,marginTop: 5}}
									source={{uri: `http://kalantari.almaseshomal.com/Mehr/${manager_pic}`}}
								/>
							</View>
						</View>
					</View>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    header: {
		width: '100%',
		flexDirection: 'column',
		alignItems: 'center',
		backgroundColor: '#f7f7f7',
		paddingVertical: 30,
		flex: 1
	},
	image: {
		width: 100,
		height: 100,
		marginBottom: 5
	},
	explainText: {
		paddingHorizontal: 20,
		marginTop: 25,
		textAlign: 'center'
	},
	lastSection: {
		width: '100%',
		paddingHorizontal: 20,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	lastSectionColumn: {
		flexDirection: 'column',
		alignItems: 'center',
		flex: 1
	},
	line: {
		width: 1,
		height: 30,
		backgroundColor: '$dark_color',
		opacity: 0.6
	}
})

export { Category }
