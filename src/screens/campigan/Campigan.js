import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Image, 
	Dimensions,
	Share
} from 'react-native';
import axios from 'axios';
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg,{
    LinearGradient,
    Rect,
    Defs,
    Stop
} from 'react-native-svg';
//compoenents
import { PageWrapper, MIcon, MText, Button } from '../../components/commons'
import { ImageSlide, CardSlider, StyledButton} from '../../components/elements'
import { numbers } from '../../utils';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width*9/10;
const ITEM_HEIGHT = ITEM_WIDTH/2;

class Campigan extends Component{    
	state = {
		loading: true,
		"id": null,
        "pic": "",
        "title": "",
        "content": "",
        "hashtag": "",
        "helpers": "",
        "category_id": null,
        "gallery": []
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				const fd = new FormData();
				fd.append("id",this.props.id);
				axios.post("/plans",fd).then(({data}) => {
					if(data.code === 200){
						this.setState({
							...data.plans,
							loading: false
						});
					}
				}).catch(err => false);
                break;
            case 'willDisappear':
                break;
        }
	}

	share = () =>{
        Share.share({
            title: "رادمهر",
			message: `
			${this.state.title}
			
			.شما هم با عضویت در اپلیکیشن رادمهر و به اشتراک گذاری این پویش ما رو در نهضت اجتماعی مهربانی یاری کنید
			لینک دانلود http://bit.ly/radmehr
			
			#${this.state.hashtag}`
        })
	}

    render(){
		const { title, pic, content, hashtag, helpers, gallery } = this.state; 

        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				loading={this.state.loading}
				title={this.props.title.substring(0,30)+"..."}
            >
				<ScrollView 
				showsVerticalScrollIndicator={false}
				contentContainerStyle={styles.container}
				ref={node => this.scrollView = node}
				style={{
					height: '100%'
				}} >
					<View style={styles.imageWrapper} >
						<Image 
							style={styles.image}
							source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
							resizeMode="cover"
						/>
						<View style={styles.absoluteWrapper} >
							<Svg
								height={ITEM_HEIGHT}
								width={ITEM_WIDTH}
							>
								<Defs>
									<LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
										<Stop offset="0%" stopColor="rgb(0,0,0)" stopOpacity="0" />
										<Stop offset="100%" stopColor="rgb(0,0,0)" stopOpacity="0.9" />
									</LinearGradient>
								</Defs>
								<Rect x="0" y="0" width={ITEM_WIDTH} height={ITEM_HEIGHT} fill="url(#grad)"  />
							</Svg>
						</View>
						<View style={[styles.absoluteWrapper,styles.textWrapper]} >
							<MText style={styles.titleText} type="bold_m" >{title}</MText>
							<MText style={styles.helpersText} type="reg_s" >{numbers.toPersianDigits(helpers*1)} نفر همیاری کردند</MText>
						</View>
						<Button style={styles.headerShare} onPress={() => this.scrollView.scrollToEnd()} >
							<MIcon 
								name="share"
								size={25}
								color={EStyleSheet.value("$ligh_color")}
							/>
						</Button>
					</View>
					<View style={[styles.row,styles.lastRow]} >
						<MText type="bold_m" >توضیحات کمپین</MText>
						<MText style={{marginTop: 10}} type="reg_b">{content}</MText>
					</View>
					<CardSlider 
						data={gallery}
						horizontal={false}
						renderItem={(item) => <ImageSlide key={item.id} {...item} />}
						title="گالری تصاویر"
						showMore={false}
					/>
					<View style={styles.footer} >
						<MText type="bold_m" style={{color: '#fff', textAlign: 'center', width: '80%',marginTop: 20}} >{`شما هم با به اشتراک گذاری ${hashtag} در فضای مجازی به جمع همیاران این کمپین بپیوندید.`}</MText>
						<StyledButton
							title="به اشتراک گذاری این کمپین"
							icon="heemayat"
							backgroundColor={EStyleSheet.value("$first_colorful")}
							iconColor={EStyleSheet.value("$dark_color")}
							iconSize={25}
							width={width*9/10}
							onPress={this.share}
						/>
					</View>
				</ScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
		paddingTop: 10,
		alignItems: 'center'
	},
	imageWrapper: {
        width: ITEM_WIDTH,
        height:  ITEM_HEIGHT,
		position: 'relative',
		borderRadius: 10,
		overflow: 'hidden',
		marginBottom: 20
    },
    image: {
        width: '100%',
        height: '100%'
	},
	headerShare: {
		position: 'absolute',
		zIndex: 40,
		bottom: 0,
		left:0,
		padding: 15
	},
    absoluteWrapper: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: 10,
        top: 0,
        right: 0
    },
    textWrapper: {
        zIndex: 20,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    titleText: {
        width: '100%',
        color: '$ligh_color'
    },
    helpersText: {
        color: '$first_colorful',
    },
	row: {
		paddingHorizontal : 7,
		paddingVertical: 15,
		marginHorizontal: '5%'
	},
	lastRow: {
		backgroundColor: '#f7f7f7',
		flexDirection: 'column',
		borderRadius: 10,
		width: '90%',
		marginBottom: 10
	},
	label: {
		flexDirection: 'row-reverse',
		alignItems: 'center',
		marginVertical: 5,
		opacity: 0.7
	},
	labelText: {
		marginRight: 6,
		fontSize: 14
	},
	footer: {
		width: '100%',
		justifyContent: 'center',
		alignItems:'center',
		backgroundColor: '$dark_color'
	}
})

export { Campigan }
