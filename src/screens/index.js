import { Navigation } from 'react-native-navigation';
//screens
import { Home } from './home';
import { NewsMore } from './newsMore';
import { Category } from './category';
import { ClientPresent } from './clientPresent';
import { Register } from './register';
import { About } from './about';
import { Campigan } from './campigan';
import { SelectModal, CheckModal, CheckNewData } from './modal';
import { Notification } from './Notification';

//register screens for navigator
export const registerScreens = () => {
    Navigation.registerComponent('Mehr.Home', () => Home);
    Navigation.registerComponent('Mehr.NewsMore', () => NewsMore);
    Navigation.registerComponent('Mehr.Category', () => Category);
    Navigation.registerComponent('Mehr.ClientPresent', () => ClientPresent);
    Navigation.registerComponent('Mehr.Register', () => Register);
    Navigation.registerComponent('Mehr.About', () => About);
    Navigation.registerComponent('Mehr.Campigan', () => Campigan);
    Navigation.registerComponent('Mehr.Modal.Select', () => SelectModal);
    Navigation.registerComponent('Mehr.Modal.Check', () => CheckModal);
    Navigation.registerComponent('Mehr.Modal.CheckNewData', () => CheckNewData);
    Navigation.registerComponent('Mehr.Notification', () => Notification);
}