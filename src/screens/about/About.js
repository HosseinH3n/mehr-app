import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Linking
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import call from 'react-native-phone-call'
//compoenents
import { PageWrapper, MText } from '../../components/commons'
import { StyledButton } from '../../components/elements';

class About extends Component{    
	state = {
		loading: true
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				this.setState({
					loading: false
				})
                break;
            case 'willDisappear':
                break;
        }
	}

    render(){
		const buttonProps = {
			color: EStyleSheet.value("$second_color"),
			backgroundColor: EStyleSheet.value("$ligh_color"),
			type: "select",
			style: {
				borderWidth: 1,
				borderColor: EStyleSheet.value("$thir_color"),
				marginVertical: 5
			}
		};

        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				loading={this.state.loading}
				title={"معرفی تیم"}
            >
				<ScrollView 
				showsVerticalScrollIndicator={false}
				contentContainerStyle={styles.container}
				style={{
					height: '100%'
				}} >
					<View style={styles.row} >
						<MText style={{marginBottom: 20}} type="bold_m" >از مردم برای مردم</MText>
						<MText type="reg_b">{`
 آیا میخواهید در کمک رسانی به نیازمندان نقشی ایفا کنید؟  با استفاده از توانمندی و تخصص های شما، به کمک هم میتوانیم نیازمندان را شناسایی و مساعدت نماییم.  شما میتوانید زمینه های تخصص خود را مشخص کنید و در آن حوزه با ما همکاری داشته باشید.   

اپلیکیشن رادمهر
مسیر کمک رسانی و استفاده از تخصص شما علاقه مندانی که مایل به همکاری با نهضت اجتماعی مهربانی هستید ، نیاز به سرعت بخشی دارد.   
اپلیکیشن رادمهر با این هدف طراحی شده و در اختیار عموم قرار گرفته است.

 برخی از سایر امکانات اپلیکیشن :
  - معرفی نیازمند در صورت شناخت - مشاهده اخرین رویداد  ها و گزارش کار ها - شرکت در طرح ها و پویش ها
نیازمند همکاری شما در این حرکت مقدس هستیم.
						`}</MText>

						<StyledButton 
							title="شماره تماس : ۰۹۰۵۷۵۷۷۰۱۴"
							{...buttonProps}
							onPress={() => {
								call({
									number: '09057577014',
									prompt: false
								}).catch(() => false)
							}}
						/>
						<StyledButton 
							title="آیدی تلگرام :‌ radmehrapp"
							{...buttonProps}
							onPress={() => {
								Linking.openURL("http://t.me/radmehrapp")
							}}
						/>
						<StyledButton 
							title="آیدی اینستاگرام :‌ radmehrapp"
							{...buttonProps}
							onPress={() => {
								Linking.openURL("http://instagram.com/radmehrapp")
							}}
						/>
					</View>
				</ScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
		marginHorizontal: '5%',
		paddingTop: 10,
		paddingBottom: 30
	},
	row: {
		paddingHorizontal : 7,
		paddingVertical: 15
	}
})

export { About }
