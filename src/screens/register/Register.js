import React, { Component } from 'react';
import {
	View,
	ScrollView,
	Image, 
	Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//compoenents
import { PageWrapper, Input, SelectBox } from '../../components/commons';
import { StyledButton } from '../../components/elements';
import { numbers, navHelper } from '../../utils';
import { registerStore, homeStore } from '../../stores';
import { observer } from 'mobx-react';
//variables
const { width } = Dimensions.get("screen");

@observer
class Register extends Component{    
	state = {
		loading: true
	}

	constructor(props){
		super(props);

		this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
	}

	onNavigatorEvent(event) {
        switch(event.id) {
			case 'didAppear':
				this.setState({
					loading: false
				});
                break;
            case 'willDisappear':
                break;
        }
	}

	register = () => {
		registerStore.handleRegister().then(() => {
			navHelper.setNotif({
				text: "ثبت نام شما با موفقیت انجام شد ، ممنون از همیاری شما",
				type: 'success',
				time: 2
			});
			navHelper.pop();
		}).catch(err => {
			navHelper.setNotif({
				text: err
			})
		})
	}

    render(){
        return(
            <PageWrapper 
                navigator={this.props.navigator}
				type="back"
				loading={this.state.loading}
				title="ثبت نام  مددکار"
				style={{flex: 1}}
            >
				<KeyboardAwareScrollView 
					showsVerticalScrollIndicator={false}
					keyboardShouldPersistTaps={"always"}
					contentContainerStyle={styles.container} >
					<Input 
						placeholder="نام و نام خانوادگی"
						handleChange={(text) => registerStore.fillForm("fullname",text)}
					/>
					<Input 
						placeholder="شماره همراه"
						keyboardType="numeric"
						handleChange={(text) => registerStore.fillForm("phone",text)}
					/>
					<View style={styles.row} >
						<Input 
							placeholder="سن"
							keyboardType="numeric"
							style={{flex: 1}}
							handleChange={(text) => registerStore.fillForm("age",text)}
						/>
						<SelectBox 
							title="جنسیت"
							style={{flex: 1,marginRight: 20}}
							data={registerStore.getGenderes}
							onSelect={(id) => registerStore.fillForm("sex",id)}
						/>
					</View>
					<View style={styles.row} >				
						<SelectBox 
							title="شهر"
							style={{flex: 1}}
							data={registerStore.getCities}
							onSelect={(id) => registerStore.fillForm("city_id",id)}
						/>
						<SelectBox 
							title="استان"
							style={{flex: 1,marginRight: 15}}
							data={registerStore.getProvinces}
							onSelect={(id) => {
								registerStore.selectProvince(id);
								registerStore.fillForm("province_id",id)
							}}
						/>
					</View>
					<View style={styles.row} >
						<Input 
							placeholder="رشته"
							style={{flex: 1}}
							handleChange={(text) => registerStore.fillForm("expertise_education",text)}
						/>
						<SelectBox 
							title="مقطع تحصیلی"
							style={{flex: 1,marginRight: 15}}
							data={registerStore.getCertificates}
							onSelect={(id) => registerStore.fillForm("period_education",id)}
						/>
					</View>
					<View style={styles.row} >
						<SelectBox 
							title="نحوه همکاری"
							style={{flex: 1}}
							data={registerStore.getAssistWays}
							onSelect={(id) => registerStore.fillForm("assist_way",id)}
						/>
						<SelectBox 
							title="کارگروه"
							content="انتخا یک یا چند کارگروه در حوزه توانایی های شما"
							style={{flex: 1}}
							style={{flex: 1,marginRight: 15}}
							data={homeStore.getCategories}
							onSelect={(id) => registerStore.fillForm("categories",id)}
						/>
					</View>
					<Input 
						placeholder="توضیح توانمندی ها و نحوه همکاری (اختیاری)"
						style={{height: 120}}
						multiline={true}
						handleChange={(text) => registerStore.fillForm("assist_description",text)}
					/>

					<View style={{
						width: '100%',
						justifyContent: 'center',
						alignItems:'center'
					}} >
						<StyledButton
							title="ثبت نام  مددکار"
							icon="heemayat"
							disabled={registerStore.loading}
							backgroundColor={EStyleSheet.value("$first_colorful")}
							iconColor={EStyleSheet.value("$dark_color")}
							iconSize={23}
							width={width*8/10}
							onPress={this.register}
						/>
					</View>
				</KeyboardAwareScrollView>
            </PageWrapper>
        )
    }
}


const styles = EStyleSheet.create({
    container: {
		marginHorizontal: '5%',
		paddingTop: 10
	},
	row: {
		flexDirection: 'row',
		width: '100%',
		alignItems: 'center'
	}
})

export { Register }
