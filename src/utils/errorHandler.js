const getError = (type,data) => {
    let errorText = "مشکلی پیش اومده ، دوباره تلاش کنید";

    if(type === "res"){
        if(data.code === 300){
            errorText = data.error;
        } else if(data.code === 301) {
            errorText = Object.values(data.error)[0][0];
        }
    } else if(type === "catch"){
        errorText = "سرور دچار مشکل شده است";
    }

    return errorText;
}

export { getError }