import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';

class NavHelper {
    navigator = null;

    setNavigator = (navigator) => {
        this.navigator = navigator;
    }

    push = ({screen,passProps = {},animationType,type = "push"}) => {
        const obj = {
            screen,
            passProps,
            navigatorStyle: {
                navBarHidden: true
            },
            animated: true,
            animationType
        };
        
        if(type === "push")
            this.navigator.push(obj)
        else if(type === "resetTo")
            this.navigator.resetTo(obj)
        
    }

    pop = () => {
        this.navigator.pop()
    }

    openModal = ({ 
        screen, 
        title, 
        content, 
        props = {}
    }) => {
        if(Platform.OS === "ios"){
            Navigation.showLightBox({
                screen,
                passProps: {...props,title,content},
                style: {
                    backgroundBlur: "none",
                    backgroundColor: "rgba(255,255,255,0.9)",
                    tapBackgroundToDismiss: true 
                }
            });
        } else{
            Navigation.showModal({
                screen,
                passProps: {...props,title,content},
                navigatorStyle: {
                    navBarBackgroundColor: 'rgba(255,255,255,0.9)',
                    navBarButtonColor: 'rgba(255,255,255,0.9)',
                    navBarHeight: 1,
                    backButtonHidden: true
                },
                animationType: 'none'
            });
        }
    }

    closeModal = () => {
        if(Platform.OS === "ios"){
            Navigation.dismissLightBox()
        } else{
            Navigation.dismissModal({
                animationType: "none"
            });
        }
    }

    setNotif = ({text,type = "error",time = 1}) => {
        this.navigator.showInAppNotification({
            screen: "Mehr.Notification",
            passProps: {
                text,
                type
            },
            autoDismissTimerSec: time
        });
    }
}
const navHelper = new NavHelper();
export { navHelper }