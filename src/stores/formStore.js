import React from 'react';
import { observable, action, isObservableArray } from 'mobx';
import { createTransformer } from 'mobx-utils';

class FormStore {
	@observable forms = [];

	getForm = createTransformer((dataArr) => {
		let returnData;
		if(dataArr.length === 1){
			this.forms.filter(item => {
				returnData = item.name == dataArr && item
			});
			return returnData;
		}

		const formData =  this.forms.filter(item => {
				return item.name == dataArr[0]
		})[0].data;
		formData.filter(item => {
			if(item.name === dataArr[1]){
				returnData = item
			}
		});
		return returnData;
	});

	getObjectData = createTransformer((formName) => {
		let data = {};
		this.forms.filter(item => item.name == formName)[0].data.map(item => {
			data[ item.name ] = item.content;
		})
		return data;
	});

	isEmpty = createTransformer(({formName,items = []}) => {
		let isEmpty = false;
		this.forms.filter(item => item.name == formName)[0].data.map(item => {
			let i = item.content;
			if(
				i === null || 
				i === undefined || 
				i === "" || 
				((Array.isArray(i) || isObservableArray(i)) && i.length === 0)
			){
				if(items.length){
					if(items.includes(item.name)){
						isEmpty = true;
						return true;
					}
				} else {
					isEmpty = true;
					return true;
				}
			}
		});
		
		return isEmpty;
	});

	hasForm = createTransformer((formName) => {
		let data = false
		this.forms.filter(item => {
			if(item.name == formName){
				data =  true
			}
		});
		return data;
	});

	@action setForm(name,single,value) {
		let newData, newObj, dataIndex;
		const obj = this.forms.filter(item => {
			return item.name == name
		})[0];
		obj.data.filter((item,index) => {
			if(item.name === single){
				newObj = {...item, content: value};
				dataIndex = index;
			}
		});
		newData = obj.data;
		let mData = newData.splice(dataIndex, 1, newObj);
		obj.data = [...newData];
	}

	@action createForm(name,data) {
		let obj = {
			name: name
		};
		let arr = [];
		data.map(item => {
			var itemObj = {};
			itemObj.name =item.name;
			itemObj.content = (item.value == null || item.value == undefined) ? null:  item.value;
			itemObj.secure = item.secure ? item.secure : false;
			arr.push(itemObj);
		});
		obj.data = arr;
		this.forms.push(obj);
		
	}

	@action clearForm(name){
		let newData = [];
		const form = this.forms.filter(item => {
			return item.name == name
		})[0];
		form.data.filter((item,index) => {
			newData.push({...item, content: null})
		});
		form.data = [...newData];
	}
}

const formStore = new FormStore();
export { formStore };

