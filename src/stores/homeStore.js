import { observable, action, computed, toJS } from 'mobx';
import axios from 'axios';
import { getError } from '../utils';

class HomeStore {
    @observable sliders = [];
    @observable categories = [];
    @observable plans = [];
    @observable events = [];
    @observable loading = true;

    @action fetchData(){
        axios.post("").then(({data}) => {
            if(data.code === 200){
            
                this.sliders = [...data.data.sliders];
                this.categories = data.data.categories;
                this.plans = [...data.data.plans];
                this.events = [...data.data.events];
                this.loading = false;
            }
        }).catch(err => false);
    }
    @action checkNewData(){
        return new Promise((resolve,reject) => {
            axios.post("/startUpMessage").then(({data}) => {
                if(data.code === 200){
                    resolve({
                        ...data.message
                    });
                } else {
                    reject()
                }
            }).catch(err => reject())
            
        })
    }

    @computed get getSlides(){
        return toJS(this.sliders)
    }
    @computed get getCategories(){
        return toJS(this.categories.map(item => {
            return {
                ...item,
                name: item.title
            }
        }))
    }
}

const homeStore = new HomeStore();
export { homeStore };
