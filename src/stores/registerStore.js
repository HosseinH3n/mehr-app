import { observable, action, computed, toJS } from 'mobx';
import * as cities from '../cities.json';
import axios from 'axios';
import { getError } from '../utils';

class RegisterStore {
    @observable province = null;
    @observable city = null;
    @observable register = new Map([
        ["fullname",  ''],
        ["phone",  ''],
        ["age",  ''],
        ["sex",  ''],
        ["province_id",  ''],
        ["city_id",  ''],
        ["period_education",  ''],
        ["expertise_education",  ''],
        ["description",  ''],
        ["categories",  ''],
        ["assist_way",  ''],
        ["assist_description",  '']
    ]);
    @observable clientPresent = new Map([
        ["fullname",  ''],
        ["tell",  ''],
        ["description",  ''],
        ["address",  ''],
        ["needing",  ''],
        ["presenter_name",  ''],
        ["presenter_tell",  '']
    ]);
    @observable loading = false;

    @action selectProvince(id){
        this.province = id;
    }
    @action selectCity(id){
        this.city = id;
    }
    @action fillForm(key,value){
        this.register.set(key,value);
    }
    @action handleRegister(){
        const fd = new FormData();
        this.register.forEach((value,key) => {
            fd.append(key, value);
        });
        this.loading = true;
        return new Promise((resolve,reject) => {
            axios.post("/helperRegisteration",fd).then(({data}) => {
                if(data.code === 200){
                    resolve();
                } else {
                    reject(getError("res",data));
                }
                this.loading = false;
            }).catch(err => {
                this.loading = false;
                reject(getError("catch",err));
            });
        })
    }
    @action fillClient(key,value){
        this.clientPresent.set(key,value);
    }
    @action handleClient(){
        const fd = new FormData();
        this.clientPresent.forEach((value,key) => {
            fd.append(key, value);
        });
        this.loading = true;
        return new Promise((resolve,reject) => {
            axios.post("/needyPresentation",fd).then(({data}) => {
                if(data.code === 200){
                    resolve();
                } else {
                    reject(getError("res",data));
                }
                this.loading = false;
            }).catch(err => {
                this.loading = false;
                reject(getError("catch",err));
            });
        })
    }

    @computed get getProvinces(){
        // const all = JSON.parse(cities);
        const provinces = cities.data.map(({id,name}) => {
            return {
                id,
                name
            }
        })
        return provinces;
    }
    @computed get getCities(){
        if(this.province){
            const city = cities.data.filter(({id}) => id === this.province)[0].cities;
            return city;
        }
        return [];
    }
    @computed get getGenderes(){
        return [
            {
                id: 1,
                name: 'مرد'
            },{
                id: 2,
                name: 'زن'
            }
        ];
    }
    @computed get getCertificates(){
        return [
            {
                id: 1,
                name: 'زیر دیپلم'
            },{
                id: 2,
                name: 'دیپلم'
            },{
                id: 3,
                name: 'کاردانی'
            },{
                id: 4,
                name: 'کارشناسی'
            },{
                id: 5,
                name: 'کارشناسی ارشد'
            },{
                id: 6,
                name: 'دکترا'
            }
        ];
    }
    @computed get getCategories(){
        return [
            {
                id: 1,
                name: 'پزشکی'
            },{
                id:2,
                name: 'آموزشی'
            },{
                id: 3,
                name: 'اقتصادی'
            },{
                id: 4,
                name: 'کشاورزی'
            }
        ]
    }
    @computed get getAssistWays(){
        return [
            {
                id: 1,
                name: "تمام وقت"
            },{
                id: 2,
                name: "نیمه وقت"
            },{
                id: 3,
                name: "پاره وقت"
            }
        ]
    }
}

const registerStore = new RegisterStore();
export { registerStore };
