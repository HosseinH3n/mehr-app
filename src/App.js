import { Navigation } from 'react-native-navigation';
import { I18nManager } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import axios from 'axios';
// //import screens
import { registerScreens } from './screens';

registerScreens();
I18nManager.allowRTL(false);
axios.defaults.baseURL = "http://kalantari.almaseshomal.com/Mehr/api";

export default class App {
    constructor(){
        this.startApp();
    }

    startApp(){
        Navigation.startSingleScreenApp({
            screen: {
                screen: 'Mehr.Home',
                navigatorStyle: {
                    navBarHidden: true
                }
            },
            appStyle: {
                orientation: 'portrait'
            },
            animationType: 'fade'
        });
    }
}

//global styles
EStyleSheet.build({
    $ligh_color: '#fff',
    $dark_color: '#373737',
    //gray colors
    $first_color: '#2c3e50',
    $second_color: '#787878',
    $thir_color: '#e6e6e6',
    //colorful
    $first_colorful : '#b6d945',
    //main fonts
    $first_font : 'YekanBakh-Regular',
    $second_font : 'YekanBakh-Heavy'
});