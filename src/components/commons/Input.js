import React,{ Component } from 'react';
import {
    Text,
    View,
    TextInput,
    Animated,
    Easing,
    Platform,
    UIManager,
    LayoutAnimation,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { observer } from 'mobx-react';
//import my compoennts
import { Iconm, MText } from '.';
import { formStore } from '../../stores';
import { Button } from './Button';

@observer
class Input extends Component{
    constructor(props){
        super(props);

        if(Platform.OS === "android"){
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
        }
        this.state = {
            value : '',
            focus: new Animated.Value(0),
            multiline: true
        }
    }

    componentWillMount = () => {
        const {formName, name} = this.props;
        if(formName){
            const content = formStore.getForm([formName,name]).content;
            content && this._labelTop(content);
        }
    }

    handleChange(text){
        if(!this.props.formName){
            this.setState({ value: text })
            if (this.props.handleChange) this.props.handleChange(text);
        } else {
            let {formName, name} = this.props;
            formStore.setForm(formName,name,text);
        }

        this._labelTop(text);
    }

    _labelTop = (text) => {
        if(text.split('').length == 0 && this.state.focus._value == 1){
            Animated.timing(
                this.state.focus,
                {
                    toValue: 0,
                    duration: 300,
                    easing: Easing.bezier(.44,.4,.28,1)
                }
            ).start();
        } else if(text.split('').length > 0 && this.state.focus._value == 0) {
            Animated.timing(
                this.state.focus,
                {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.bezier(.44,.4,.28,1)
                }
            ).start();
        }
    }

    _handleFocus = () => {
        if(!this.props.multiline){
            this.setState({
                multiline: false
            })
        }
    }

    _handleBlur = () => {
        this.setState({
            multiline: true
        });
    }

    onContentSizeChange = (event) => {
        let { height } = event.nativeEvent.contentSize;
        const { inputHeight } = this.props;

        this.setState({
            inputHeight: inputHeight ? inputHeight : Math.max(
                16 * 1.5,
                Math.ceil(height) + Platform.select({ ios: 5, android: 1 })
            ),
        });
    }

    render(){
        let {
            color = EStyleSheet.value('$dark_color'),
            textColor = color,
            icon,
            placeholderColor =  EStyleSheet.value('$second_color'),
            size = 16,
            placeholder = '',
            autoFocus = false,
            align = "right",
            style = {},
            keyboardType = 'default',
            returnKeyType = "done",
            secureTextEntry = false,
            formName,
            name,
            value = this.state.value,
            multiline = false,
            onSubmitEditing
        } = this.props;

        if(formName){
            const formInfo = formStore.getForm([formName,name]);
            value = formName ? formInfo.content : this.state.value;
            secureTextEntry = formInfo.secure && formInfo.secure;
        }

        let translateY = this.state.focus.interpolate({
            inputRange: [0, 1],
            outputRange: [15, 0],
        });
        let height = this.state.focus.interpolate({
            inputRange: [0, 1],
            outputRange: [this.state.inputHeight, 15],
        });
        let font = this.state.focus.interpolate({
            inputRange: [0, 1],
            outputRange: [15, 12],
        });

        return(
            <View style={[styles.container,style]}>
                {icon && <Iconm name={icon} size={size} color={color} style={styles.icon} />}

                <View style={styles.inputWrapper} >
                    <TextInput
                        style={[styles.input,{color: textColor, fontSize: size, textAlign: align, height: multiline ? '100%' : this.state.inputHeight }]}
                        onChangeText={(text) => this.handleChange(text)}
                        value={value}
                        autoFocus={autoFocus}
                        selectionColor={EStyleSheet.value('$first_colorful')}
                        autoCorrect={false}
                        returnKeyType={returnKeyType}
                        onContentSizeChange={this.onContentSizeChange}
                        keyboardType={keyboardType}
                        multiline={this.state.multiline}
                        secureTextEntry={secureTextEntry}
                        underlineColorAndroid={"rgba(0,0,0,0)"}
                        onFocus={this._handleFocus}
                        onBlur={this._handleBlur}
                        onSubmitEditing={onSubmitEditing}
                    />
                </View>
 
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    transform: [{
                        translateY: translateY
                    }],
                    right: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: height,
                    zIndex: 1,
                }} >
                    <Animated.Text style={{
                        color : placeholderColor,
                        fontFamily: EStyleSheet.value('$first_font'),
                        fontSize: font
                    }} >{placeholder}
                    </Animated.Text>
                </Animated.View>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomColor: '$thir_color',
        borderBottomWidth: 1,
        paddingRight: 5,
        paddingLeft: 5,
        position: 'relative',
        marginTop: 15
    },
    inputWrapper: {
        flex: 9,
        position: 'relative',
        zIndex: 10,
        paddingTop: 15
    },
    input: {
        fontFamily: '$first_font',
        padding: 5,
        textAlignVertical: "top",
        zIndex: 10,
        maxHeight: '100%'
    },
    icon: {
        flex: 1,
        textAlign: 'center'
    }
});

export {Input};
