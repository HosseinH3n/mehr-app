import React, { PureComponent } from 'react';
import {
    View,
    Animated,
    Easing,
    Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import { NavBar, StatusView } from '.';
import { navHelper } from '../../utils';
//variables
const { height } = Dimensions.get("screen");

class PageWrapper extends PureComponent{
    constructor(props){
        super(props);

        navHelper.setNavigator(props.navigator);
    }

    state = {
        loading: false,
        showModal: false,
        modalAnimate: new Animated.Value(0)
    }

    componentWillMount = () => {
        this.setState({
            loading: this.props.loading
        })
    }

    componentWillReceiveProps(newProps){
        this.setState({
            loading: newProps.loading
        });
    }

    toggleAnimation = () => {
        const {showModal} = this.state;
        const runAnimation = (callback) =>{
            Animated.timing(this.state.modalAnimate,{
                toValue: showModal ? 0 : 1,
                duration: 400,
                easing: Easing.bezier(.26,.33,.4,1)
            }).start(callback);
        }
        
        if(showModal){
            runAnimation(() => {
                this.setState({
                    showModal: false
                });
            });
        } else {
            this.setState({
                showModal: true
            });
            requestAnimationFrame(() => {
                runAnimation()
            });
        }
    }

    renderChild = () => {
        const { loading } = this.state;
        const { 
            children
         } = this.props;

        if(loading){
            return <StatusView type="pending" />
        }

        return children;
    }

    render(){
        const {
            style = {}, 
            wrapperStyle = {},
            type,
            title = ""
        } = this.props;
        const { showModal, modalAnimate } = this.state;
        const backgroundColor = modalAnimate.interpolate({
            inputRange: [0, 1],
            outputRange: ['rgba(0,0,0,0)','rgba(0,0,0,0.8)']
        });
        const heightt = modalAnimate.interpolate({
            inputRange: [0, 1],
            outputRange: [0,height*4/10]
        });

        return(
            <View style={[{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: EStyleSheet.value('$ligh_color')
            },style]} >
                {type && <NavBar type={type} title={title} toggleModal={this.toggleAnimation} showModal={showModal} />}
                <View style={{
                    flex: 1,
                    position: 'relative'
                }} >
                    {showModal ? (
                        <Animated.View style={{
                            width: '100%',
                            height: '100%',
                            position: 'absolute',
                            backgroundColor,
                            top: 0,
                            zIndex: 10,
                            right: 0
                        }} >
                            <Animated.View style={{
                                width: '100%',
                                backgroundColor: '#fff',
                                height: heightt
                            }} >
                                {this.props.menuComponent(this.toggleAnimation)}
                            </Animated.View>
                        </Animated.View>
                    ) : null}
                    <View style={[{flex: 1},wrapperStyle]}  >
                        {this.renderChild()} 
                    </View>
                </View>
            </View>
        ) 
    }
}

export { PageWrapper }
