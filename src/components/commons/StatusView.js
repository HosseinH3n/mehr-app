import React, {PureComponent} from 'react';
import { View, ActivityIndicator } from'react-native';
// import Spinner from 'react-native-spinkit';
import EStyleSheet from 'react-native-extended-stylesheet';

class StatusView extends PureComponent{
    render(){
        const { type = "pending" } = this.props;
        let content = null;

        switch(type){
            case "pending":
                content = <View style={{flex: 1,justifyContent: 'center',alignItems: 'center'}} ><ActivityIndicator size="small" color={EStyleSheet.value("$first_color")} /></View>;
                break;
        }
        
        return content;
    }
}

export { StatusView };
