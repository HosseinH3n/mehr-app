import React from 'react';
import { Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const MText = (props) => {
    let { children, style, type = "reg_m" } = props;
    let typeStyle ;

    switch(type){
        case "bold_m": 
            typeStyle = styles.bold_m;
            break;
        case "bold_s": 
            typeStyle = styles.bold_s;
            break;
        case "reg_b": 
            typeStyle = styles.reg_b;
            break;
        case "reg_m":
            typeStyle = {};
            break;
        case "reg_s":
            typeStyle = styles.reg_s;
            break;
    }

    return <Text style={[styles.text,typeStyle,style]} >{children}</Text>
};

const styles = EStyleSheet.create({
    text: {
        color : '$dark_color',
        fontFamily: '$first_font',
        fontSize: 15
    },
    bold_m: {
        color : '$first_color',
        fontFamily: '$second_font',
        fontSize: 18
    },
    bold_s: {
        fontFamily: '$second_font',
        fontSize: 16
    },
    reg_b: {
        fontSize: 17
    },
    reg_s: {
        color : '$second_color',
        fontSize: 13
    }
});

export {MText};
