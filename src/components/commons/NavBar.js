import React, {PureComponent} from 'react';
import { 
    View,
    StatusBar,
    Platform
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import { 
    Button,
    MIcon 
} from '.';
import { navHelper } from '../../utils';
import { MText } from './MText';

class NavBar extends PureComponent{
    state = {
        showModal: false
    }

    componentWillMount = () => {
        this.setState({
            showModal: this.props.showModal
        })
    }

    componentWillReceiveProps(newProps){
        this.setState({
            showModal: newProps.showModal
        });
    }

    openRegister(){
        navHelper.push({
            screen: 'Mehr.Register'
        })
    }

    render(){
        const { type = "back", title } = this.props;
        let content = null;

        switch(type){
            case "back":
                content = (
                    <Button onPress={navHelper.pop} style={[styles.button,styles.headerBack]} >
                        <MIcon 
                            name="back" 
                            size={16} 
                            color={EStyleSheet.value("$dark_color")} 
                            style={{
                                transform: [{
                                    rotate: "180deg"
                                }],
                                marginHorizontal: 6
                            }}
                        />
                        <MText type="bold_m" >{title} </MText>
                    </Button>
                );
                break;
            case "home":
                content = (
                    <View style={{
                        flex: 1,
                        justifyContent: 'space-between',
                        flexDirection: 'row-reverse',
                        alignItems: 'center'
                    }} >
                        <Button onPress={() => this.props.toggleModal()} style={[styles.button,styles.menu]} >
                            <MIcon 
                                name={this.state.showModal ? "close" : "group"}
                                size={22} 
                                color={EStyleSheet.value("$dark_color")}
                            />
                        </Button>
                        <MText style={{fontSize : 24}} type="bold_m" >رادمهر</MText>
                        <Button style={styles.button} onPress={this.openRegister} >
                            <MIcon 
                                name={"heemayat"}
                                size={24} 
                                color={EStyleSheet.value("$first_colorful")}
                            />
                        </Button>
                    </View>
                );
                break;
        }
        return(
            <View style={styles.container} >
                <StatusBar
                    backgroundColor="#000"
                    barStyle="light-content"
                    translucent={false}
                />
                <View style={styles.main}>
                    {content}
                </View>
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        width: '100%',
        height: 58,
        position: 'relative',
        marginTop: Platform.select({
            ios: 23,
            android: 0
        }),
        borderBottomColor: '$thir_color',
        borderBottomWidth: 1
    },
    main: {
        flex: 1,
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
        zIndex: 10
    },
    button: {
        height: 58, 
        width: 58 , 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    headerBack: {width: 'auto',flexDirection: 'row-reverse',alignItems: 'center'}
});

export { NavBar };
