import React, { PureComponent } from 'react';
import { 
    TouchableHighlight, 
    TouchableOpacity, 
    TouchableNativeFeedback, 
    TouchableWithoutFeedback,
    UIManager,
    Platform,
    LayoutAnimation
} from 'react-native';
// import Spinner from 'react-native-spinkit';
import EStyleSheet from 'react-native-extended-stylesheet';

class Button extends PureComponent{
    state = {
        loading: false
    }

    constructor(props){
        super(props);

        if(Platform.OS === "android"){
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            loading: nextProps.loading
        });
        var CustomLayoutSpring = {
            duration: 200,
            create: {
                type: LayoutAnimation.Types.easeIn,
                property: LayoutAnimation.Properties.opacity
            },
            update: {
                type: LayoutAnimation.Types.easeIn
            },
        };
        LayoutAnimation.configureNext(CustomLayoutSpring);
    }

    render(){
        let { 
            children, 
            style, 
            type = "opacity", 
            onPress,
            loading,
            disabled = false,
            onLongPress = () => false,
            loadingColor = EStyleSheet.value("$first_colorful")
        } = this.props;
        let spinner = null;
        let button = null ;
    
        switch(type){
            case "opacity":
                button = (
                    <TouchableOpacity disabled={disabled} style={style} onLongPress={onLongPress} onPress={onPress} activeOpacity={0.9}>
                        {loading ? spinner : children}
                    </TouchableOpacity>
                );
                break;
            case "native": 
                button = (
                    <TouchableNativeFeedback disabled={disabled} style={style} onLongPress={onLongPress} onPress={onPress}>
                        {loading ? spinner : children}
                    </TouchableNativeFeedback>
                );
                break;
            case "highlight":
                button = (
                    <TouchableHighlight disabled={disabled}  onPress={onPress} onLongPress={onLongPress} style={style}>
                        {loading ? spinner : children}
                    </TouchableHighlight>
                );
                break;
            case "without": 
                button = (
                    <TouchableWithoutFeedback disabled={disabled} onPress={onPress} onLongPress={onLongPress} style={style}>
                        {loading ? spinner : children}
                    </TouchableWithoutFeedback>
                );
                break;
            default:
                typeStyle = {};
                break;
        }
    
        return button;
    }
}
export {Button};
