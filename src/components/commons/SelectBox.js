import React,{ Component } from 'react';
import { View, ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//import my compoennts 
import { MIcon, Button, MText } from '.';
import { navHelper } from '../../utils';

class SelectBox extends Component{
    constructor(props){
        super(props);

        this.state = {
            selectedItem : "",
            selectedItems: []
        }
    }

    handleOpenModal = () => {
        let { 
            title,
            data,
            content,
            multiSelect = false
        } = this.props;

        navHelper.openModal({
            screen: "Mehr.Modal.Select",
            title,
            content,
            props: {
                data,
                multiSelect,
                onSelect: (item) => this.selectItem(item)
            }
        })
    }

    selectItem = (item) => {
        const {multiSelect = false} = this.props;

        navHelper.closeModal();
        if(multiSelect){
            this.setState({
                selectedItems: item
            });
            this.props.onSelect && this.props.onSelect(item);
        } else {
            this.setState({
                selectedItem: item.name
            })
            this.props.onSelect && this.props.onSelect(item.id);
        }
    }

    render(){
        let { 
            title,
            style = {},
            multiSelect = false
        } = this.props;
        const { selectedItem,selectedItems } = this.state;

        return(
            <Button style={[styles.container,style]} onPress={this.handleOpenModal}>
                <MText style={{fontSize: selectedItem ? 12 : 15}} type="reg_m">{title}</MText>
                <View style={{flexDirection: 'row-reverse',alignItems: 'center'}} >
                    {selectedItem ? <MText style={{marginLeft: 5}}  type="bold_m">{selectedItem}</MText> : null}
                    {selectedItems.length ? (
                        <ScrollView 
                            horizontal={true} 
                            showsHorizontalScrollIndicator={false}
                            style={{width: '80%'}} 
                        >   
                            {selectedItems.map(item => <MText key={item.id} style={{marginLeft: 5}}  type="bold_m">{item.name}</MText>)}
                        </ScrollView>
                    ) : null}
                    <MIcon size={11} name="back" color={EStyleSheet.value("$thir_color")} />
                </View>
            </Button>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        borderBottomWidth: 1,
        padding: 5,
        borderBottomColor: '$thir_color',
        width: '100%',
        justifyContent: 'space-between',
        marginTop: 15,
        paddingTop: 20
    },
    icon: {
        flex: 1,
        textAlign: 'center'
    }
});

export {SelectBox};
