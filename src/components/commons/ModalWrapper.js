import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Platform,
    Animated,
    Easing,
    UIManager,
    LayoutAnimation
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import { MText } from '.';
import { Button } from './Button';
import { navHelper } from '../../utils';
//variables
const { width, height } = Dimensions.get('window');

class ModalWrapper extends Component{
    state = {
        showText: new Animated.Value(0),
        showContent: false
    }

    constructor(props){
        super(props);

        if(Platform.OS === "android"){
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
        }
    }

    componentDidMount = () => {
        Animated.timing(this.state.showText, {
            toValue: 1,
            duration: 400,
            easing: Easing.bezier(.28,.45,.7,1)
        }).start();
        setTimeout(() => {
            this.setState({
                showContent: true
            })
            var CustomLayoutSpring = {
                duration: 250,
                create: {
                    type: LayoutAnimation.Types.easeIn,
                    property: LayoutAnimation.Properties.opacity
                },
                update: {
                    type: LayoutAnimation.Types.easeIn
                },
            };
            LayoutAnimation.configureNext(CustomLayoutSpring);
        },300)
    }
    
    render(){
        const { title, content, children, style = {} } = this.props;
        const { showContent,showText } = this.state;
        let textStyle = {
            opacity: showText,
            transform: [{
                translateY: showText.interpolate({
                    inputRange: [0 , 1],
                    outputRange: [12, 0]
                })
            }]
        };

        const body = (
            <View style={styles.container}>
                <View View style={[styles.header,{alignItems: "flex-end"}]}>
                    <Animated.View style={textStyle} >
                        <MText 
                            type="bold_m" 
                            style={{
                                color: EStyleSheet.value("$ligh_color"),
                                fontSize: 20
                            }} 
                        >
                            {title}
                        </MText>
                    </Animated.View>
                    <Animated.View style={textStyle}>
                        <MText 
                            type="reg_m"
                            style={{
                                color: EStyleSheet.value("$ligh_color"),
                                opacity: 0.8,
                                marginTop: 5
                            }}
                        >
                            {content}
                        </MText>
                    </Animated.View>
                </View>
                <View style={[styles.content,style]}>{showContent ? children : null}</View>
            </View>
        );

        if(Platform.OS === "ios"){
            return body;
        }
        return(
            <View style={styles.wrapper} >
                {body}
                <Button onPress={() => navHelper.closeModal()} style={styles.modalBack} />
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    wrapper:{
        flex: 1,
        width: width,
        height: height,
        backgroundColor: 'rgba(255,255,255,0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flexDirection: 'column',
        width: width*85/100,
        backgroundColor: '$dark_color',
        borderRadius: 10,
        padding: 15,
        position: 'relative',
        zIndex: 20
    },
    header: {
        width: '100%',
        padding: '5%',
        flexDirection: 'column'
    },
    content: {
        width: '100%',
        flexDirection: 'column'
    },
    modalBack: {
        width: '100%', 
        height: '100%', 
        position: 'absolute', 
        zIndex: 1
    }
});

export {ModalWrapper};
