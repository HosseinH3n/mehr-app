import React from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from './selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
//my custom icon
const MIcon = ({ name, size, color, style}) => {
    return <Icon name={name} size={size} color={color} style={style} />;
};

export {MIcon};
