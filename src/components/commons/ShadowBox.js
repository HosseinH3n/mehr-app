import React from 'react';
import {
    View
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { BoxShadow } from 'react-native-shadow';

const ShadowBox = (props) => {
    const { 
        children,
        width,
        height,
        radius = 0,
        shadowBlur = 20,
        shadowColor = EStyleSheet.value('$first_color'),
        shadowOpacity = 0.2,
        shadowX = 0,
        shadowY = 8,
        depth = 20,
        backgroundColor = EStyleSheet.value('$ligh_color'),
        style,
        containerStyle
    } = props;

    return (
            <BoxShadow setting={{
                width: width,
                height,
                color: shadowColor,
                border: shadowBlur,
                radius,
                opacity: shadowOpacity,
                x:shadowX,
                y:shadowY,
                style: {...style}
            }}>
                <View style={[{
                    width,
                    height,
                    borderRadius: radius,
                    backgroundColor,
                    overflow:"hidden",
                    position:"relative"
                },containerStyle]} >{children}</View>
            </BoxShadow>
    );
};

export {ShadowBox};