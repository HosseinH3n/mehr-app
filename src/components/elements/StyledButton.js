import React, { PureComponent } from 'react';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//components
import { 
    Button, 
    MText,
    MIcon,
    ShadowBox
} from '../commons';

class StyledButton extends PureComponent {
    render(){
        const { 
            style = {},
            type = "colory",
            title,
            icon,
            size = 17,
            width,
            margin,
            iconSize = size,
            fontSize = size,
            onPress,
            disabled = false,
            loading,
            color = EStyleSheet.value("$ligh_color"),
            iconColor= EStyleSheet.value("$first_colorful"),
            backgroundColor = EStyleSheet.value("$first_color"),
            loadingColor = color
        } = this.props;
        let element, elementStyle = {} ;
    
        switch(type){
            case "colory": 
                const shadowButton = {
                    width: width,
                    height : 60,
                    radius : 10,
                    backgroundColor,
                    shadowColor:backgroundColor,
                    containerStyle: styles.buttonCenter,
                    style: {
                        margin: 30
                    }
                };

                element = (
                    <ShadowBox  {...shadowButton} >
                            {icon ? (
                                <MIcon 
                                    name={icon}
                                    size={iconSize}
                                    color={iconColor}
                                    style={{marginRight: title ? 15 : 0 }}
                                />
                            ) : null}
                            {title ? (
                                <MText type="reg_m" style={{
                                    color,
                                    fontSize
                                }} >
                                    {title}
                                </MText>
                            ) : null}
                    </ShadowBox>

                );
                break;
            case "select": 
                element = (
                    <View style={styles.buttonCenter} >
                        {icon ? (
                            <MIcon 
                                name={icon}
                                size={iconSize}
                                color={iconColor}
                                style={{marginRight: title ? 10 : 0 }}
                            />
                        ) : null}
                        {title ? (
                            <MText type="reg_m" style={{
                                color,
                                fontSize
                            }} >
                                {title}
                            </MText>
                        ) : null}
                    </View>

                );
                elementStyle = [styles.colored,{
                    backgroundColor,
                    opacity: disabled ? 0.5 : 1
                }];
                break;
        }
    
        return (
            <Button 
                loading={loading} 
                disabled={disabled} 
                onPress={onPress} 
                loadingColor={loadingColor}
                style={[elementStyle,style]} 
            >
                {element}
            </Button>
        )
    }
}   

const styles = EStyleSheet.create({
    buttonCenter: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    colored: {
        padding: 10,
        borderRadius: 8
    }
});

export {StyledButton};
