import React, { PureComponent } from 'react';
import { View, Image, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width/2;

class ImageSlide extends PureComponent{
    render(){
        const {pic} = this.props;
        return(
            <View style={styles.container}>
                <Image 
                    style={styles.image}
                    source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
                    resizeMode="cover"
                />
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        borderColor : '$thir_color',
        borderWidth: 1,
        margin: 5,
        borderRadius: 8,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: ITEM_WIDTH,
    }
})


export { ImageSlide }