import React, { PureComponent } from 'react';
import { View, Image, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg,{
    LinearGradient,
    Rect,
    Defs,
    Stop
} from 'react-native-svg';
//compoenents
import { MText, MIcon, Button } from '../commons';
import { navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width*7/10;
const ITEM_HEIGHT = ITEM_WIDTH/2;

class NewsCard extends PureComponent{
    render(){
        const {id, pic, title, content} = this.props;
        return(
            <Button onPress={() => navHelper.push({
                screen: "Mehr.NewsMore",
                passProps: {
                    id,
                    title
                }
            })} style={styles.container}>
                <View style={styles.imageWrapper} >
                    <Image 
                        style={styles.image}
                        source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
                        resizeMode="cover"
                    />
                    <View style={styles.absoluteWrapper} >
                        <Svg
                            height={ITEM_HEIGHT}
                            width={ITEM_WIDTH}
                        >
                            <Defs>
                                <LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
                                    <Stop offset="0%" stopColor="rgb(0,0,0)" stopOpacity="0" />
                                    <Stop offset="100%" stopColor="rgb(0,0,0)" stopOpacity="0.9" />
                                </LinearGradient>
                            </Defs>
                            <Rect x="0" y="0" width={ITEM_WIDTH} height={ITEM_HEIGHT} fill="url(#grad)"  />
                        </Svg>
                    </View>
                    <View style={[styles.absoluteWrapper,styles.textWrapper]} >
                        <MText style={styles.titleText} type="reg_m" >{title}</MText>
                    </View>
                </View>
                <View style={{
                    padding: 7,
                    flex: 1,
                    alignItems: 'flex-end',
                    justifyContent: 'space-around'
                }} >
                    <MText type="reg_s">{content.substring(0,100)} ...</MText>
                    <MIcon 
                        name="arrow-left"
                        size={22}
                        style={{marginTop: 5}}
                        color={EStyleSheet.value("$thir_color")}
                    />
                </View>
            </Button>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'column',
        borderRadius: 8,
        overflow: 'hidden',
        borderColor : '$thir_color',
        borderWidth: 1,
        width: ITEM_WIDTH,
        margin: 5
    },
    imageWrapper: {
        width: '100%',
        height:  ITEM_HEIGHT,
        position: 'relative'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    absoluteWrapper: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: 10,
        top: 0,
        right: 0
    },
    textWrapper: {
        zIndex: 20,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    titleText: {
        width: '100%',
        color: '$ligh_color'
    },
})


export { NewsCard }