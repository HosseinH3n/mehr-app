import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Linking
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
//components
import { Button } from '../commons';
import { navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");

class Swiper extends Component {
    handleLink = (link) => {
    }

    _renderItem ({item, index}, parallaxProps) {
        return (
            <Button style={styles.slide}>
                <ParallaxImage
                    source={{uri: `http://kalantari.almaseshomal.com/Mehr/${item.pic}`}}
                    containerStyle={styles.imageContainer}
                    spinnerColor={EStyleSheet.value("$first_color")}
                    style={styles.image}
                    parallaxFactor={0.2}
                    {...parallaxProps}
                />
            </Button>
        );
    }

    render () {
        const {data} = this.props;

        return (
            <View style={styles.container} >
                <Carousel
                    ref={(c) => { this._carousel = c; }}
                    data={data}
                    hasParallaxImages={true}
                    renderItem={this._renderItem}
                    sliderWidth={width}
                    itemWidth={width*9/10}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={0.6}
                    activeAnimationType={'spring'}
                    autoplay={true}
                    loop={true}
                    autoplayDelay={2000}
                    autoplayInterval={4000}
                    contentContainerCustomStyle={{alignItems: 'center'}}
                />
            </View>
        );
    }
}

const styles = EStyleSheet.create({
    container: {
        width: '100%',
        height:  width*45/100,
        marginVertical: 10
    },
    slide: {
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%"
    },
    imageContainer: {
        width: '96%',
        height: '100%',
        borderRadius: 5,
        overflow: 'hidden',
        marginVertical: '2%'
    },
    image: {
        flex: 1,
        resizeMode: 'cover'
    }
});

export {Swiper};