import React, { PureComponent } from 'react';
import { View, Image, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//compoenents
import { MText, MIcon, Button } from '../commons';
import { numbers, navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width/2;

class CategoryCard extends PureComponent{
    render(){
        const {id, title, pic} = this.props;
        return(
            <Button onPress={() => navHelper.push({
                screen: "Mehr.Category",
                passProps: {
                    id,
                    title
                }
            })} style={styles.container}>
                <Image 
                    style={styles.image}
                    source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
                    resizeMode="cover"
                />
                <MText type="reg_b" >{title}</MText>
            </Button>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'row-reverse',
        borderRadius: 8,
        padding: 20,
        paddingRight: 30,
        borderColor : '$thir_color',
        borderWidth: 1,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 25,
        height: 25,
        marginLeft: 10
    },
    text: {
        color :'$ligh_color',
        fontSize: 17
    }
})


export { CategoryCard }