import React,{ PureComponent } from 'react';
import {
    View,
    ScrollView,
    Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//import my components
import { 
    MText,
    Button,
    MIcon
} from '../commons';
const { width } = Dimensions.get("screen");

class CardSlider extends PureComponent{
    componentDidMount = () => {
        if(this.props.data.length > 0){
            setTimeout(() => this.scrollView.scrollToEnd({animated: false}) , 0)
        }
    }
    
    render(){
        const { data, renderItem, title, horizontal= true ,onPress, showMore = true} = this.props;
        //make rtl after load data
        if(data.length === 0 ) return null
        
        return(
            <View style={styles.sliderView}>
                <Button onPress={onPress} style={styles.sliderHeader}>
                    <MText type="bold_m">{title}</MText>
                    {showMore ? (
                        <View style={{
                            flexDirection: 'row-reverse',
                            alignItems: 'center'
                        }} >
                            <MText>مشاهده همه</MText>
                            <MIcon 
                                size={11}
                                color={EStyleSheet.value("$dark_color")}
                                name="back"
                            />
                        </View>
                    ) : null}
                    
                </Button>
                <ScrollView 
                    contentContainerStyle={[{ paddingHorizontal: 10 },data.length === 1 &&{
                        width,
                        justifyContent: 'flex-end'
                    }]} 
                    horizontal={horizontal} 
                    showsHorizontalScrollIndicator={false} 
                    showsVerticalScrollIndicator={false} 
                    ref={node => this.scrollView = node}
                >
                    {data.map((item,index) => renderItem(item,index))}
                </ScrollView>
            </View>
        )
    }
}


const styles = EStyleSheet.create({
    sliderView: {
        width: '100%',
        marginVertical: 10
    },
    sliderHeader: {
        width: '100%',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    }
});

export {CardSlider};
