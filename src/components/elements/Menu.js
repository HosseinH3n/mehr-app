import React, { PureComponent } from 'react';
import { 
    View,
    Image,
    Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
//compoenents
import { MText, MIcon, Button } from '../commons';
import { navHelper } from '../../utils';
//variables
const { height } = Dimensions.get("screen");

class Menu extends PureComponent{
    menu = [
        {
            id: 1,
            title: 'ثبت نام به عنوان مددکار',
            screen: 'Mehr.Register'
        },{
            id: 2,
            title: 'معرفی مددجو',
            screen: 'Mehr.ClientPresent'
        },{
            id: 3,
            title: 'معرفی تیم ما',
            screen: 'Mehr.About'
        }
    ];

    itemPress({screen,done}){
        if(screen){
            navHelper.push({
                screen
            });
        } else if(done){
            done()
        }
        this.props.toggleAnimation();
    }

    checkVersion(){
        navHelper.openModal({
            screen: 'Mehr.Modal.Check',
            title: 'بروزرسانی',
            content: 'اپلیکیشن شما به آخرین ورژن بروزرسانی شده است.'
        })
    }

    render(){
        return(
            <View style={styles.container} >
                {/*<View style={[styles.row,{
                    flex: 3,
                    padding: 25
                }]} >
                    <Image 
                        source={require('../../assets/theme/profile.jpg')}
                        style={{
                            width: 80,
                            height: 80,
                            borderRadius: 40,
                            marginBottom: 10
                        }}
                    />
                    <MText type="bold_m" >حسین شعبانی</MText>
                    </View>*/}
                {this.menu.map(item => (
                    <Button onPress={() => this.itemPress(item)} key={item.id} style={styles.row} >
                        <MText type="reg_m" >{item.title}</MText>
                    </Button>
                ))}
            </View>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'column',
        width: '100%',
        height: height*4/10
    },
    row :{
        flex: 1,
        padding: 12,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '$thir_color',
        borderBottomWidth: 1
    }
})


export { Menu }