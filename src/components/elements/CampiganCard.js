import React, { PureComponent } from 'react';
import { View, Image, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg,{
    LinearGradient,
    Rect,
    Defs,
    Stop
} from 'react-native-svg';
//compoenents
import { MText, MIcon,Button } from '../commons';
import { numbers, navHelper } from '../../utils';
//variables
const { width } = Dimensions.get("screen");
const ITEM_WIDTH = width/2;
const ITEM_HEIGHT = width/3;

class CampiganCard extends PureComponent{
    handleCampigan(){
        
    }

    render(){
        let {id, pic, title, helpers} = this.props;
        helpers = helpers*1;

        return(
            <Button onPress={() => {
                navHelper.push({
                    screen: 'Mehr.Campigan',
                    passProps: {
                        id,
                        title
                    }
                })
            }} style={styles.container}>
                <View style={styles.imageWrapper} >
                    <Image 
                        style={styles.image}
                        source={{uri: `http://kalantari.almaseshomal.com/Mehr/${pic}`}}
                        resizeMode="cover"
                    />
                    <View style={styles.absoluteWrapper} >
                        <Svg
                            height={ITEM_HEIGHT}
                            width={ITEM_WIDTH}
                        >
                            <Defs>
                                <LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
                                    <Stop offset="0%" stopColor="rgb(0,0,0)" stopOpacity="0" />
                                    <Stop offset="100%" stopColor="rgb(0,0,0)" stopOpacity="0.9" />
                                </LinearGradient>
                            </Defs>
                            <Rect x="0" y="0" width={ITEM_WIDTH} height={ITEM_HEIGHT} fill="url(#grad)"  />
                        </Svg>
                    </View>
                    <View style={[styles.absoluteWrapper,styles.textWrapper]} >
                        <MText style={styles.titleText} type="reg_m" >{title}</MText>
                        <MText style={styles.helpersText} type="reg_s" >{numbers.toPersianDigits(helpers)} نفر همیاری کردند</MText>
                    </View>
                </View>
                <View style={styles.bottom} >
                    <MText style={{marginLeft: 10, opacity: 0.5}} type="reg_m" >شما {numbers.toPersianDigits(helpers+1)} امین نفر باشید</MText>
                    <MIcon 
                        name="arrow-left"
                        size={15}
                        style={{marginTop: 5}}
                        color={EStyleSheet.value("$thir_color")}
                    />
                </View>
            </Button>
        )
    }
}

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'column',
        borderRadius: 8,
        overflow: 'hidden',
        borderColor : '$thir_color',
        borderWidth: 1,
        width: ITEM_WIDTH,
        margin: 5,
        alignItems: 'center'
    },
    imageWrapper: {
        width: '100%',
        height:  ITEM_HEIGHT,
        position: 'relative'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    absoluteWrapper: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: 10,
        top: 0,
        right: 0
    },
    textWrapper: {
        zIndex: 20,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    titleText: {
        width: '100%',
        color: '$ligh_color'
    },
    helpersText: {
        color: '$first_colorful',
    },
    bottom: {
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        paddingVertical: 20
    }
})


export { CampiganCard }